\section{Analysis}
\label{sec:analysis}

The following section aims at describing the analysis of the data taken with the setup described in \autoref{sec:setup}.
It consists of three parts: The first part is about the extraction of relevant event parameters out of the measured signals (waveforms). The second part describes the determination of the pion and muon lifetimes and the third part describes the attempt to determine the branching ratios of \pimu and \pie via energy spectrum analysis.

\subsection{Low-level Analysis}
\label{subsec:lowlevel_analysis}
% Author: Dominick
Obtaining parameters from signals within the recorded waveforms amounts to determining the baseline and recognizing excursions from it (called "peaks" in the following).
Depending on which kind of signal one is looking at, size and position of those peaks contain certain information about an event. For example, the area of a peak in a calorimeter waveform caused by a particle passing through the calorimeter is, in good approximation, proportional to the energy deposited by the particle. As another example, the time differences measured by the TACs are encoded in the height of their output signals.

\subsubsection{Baseline Estimation}
Baselines are estimated by fitting a quartic polynomial onto all waveform samples not in the vicinity of a peak.
The resulting function is then used for baseline subtraction.
The reason for such a rather complex method being employed is the presence of low-frequency\footnote{Relative to the sampling rate.} noise ($< 200 \textrm{kHz}$) causing baseline fluctuation (\autoref{fig:pfinder_ex}, top left).
The polynomial matches the actual baseline quite well in almost all cases, which has been checked by looking at least at 50 to 100 different waveforms per logic box channel.
For a negligible amount of events ($< 1.4\cdot10^{-4}\%$), the fit does not converge, and the corresponding events are discarded.

\subsubsection{Peak Finding}
In order to detect peaks, a peak-finding algorithm computes the difference between a baseline sample and the sample preceding it (\autoref{fig:pfinder_ex}, bottom).
If the difference passes a certain threshold (positive for positive or negative for negative peak searching), the peak finder becomes active and starts looking for the difference changing its sign, which is done by checking if another threshold, which has the opposite sign of the first, has been passed.
Finally, after a change in sign has been registered, the peak finder looks for the maximum or minimum value of the waveform between the locations of both threshold crossings and saves its position as the peak's position.
Calorimeter waveforms need to be smoothed with a central floating mean filter before their difference is calculated due to high-frequency noise.
To determine peak parameters, such as peak areas etc., however, the raw waveform is always used.

\begin{figure}[h]
\centering
\begin{minipage}{0.49\textwidth}
	\includegraphics[width=\textwidth]{calo_wvfm_bl}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\includegraphics[width=\textwidth]{calo_wvfm_smooth}
\end{minipage}
\\[1em]
\begin{minipage}{0.5\textwidth}
	\includegraphics[width=\textwidth]{calo_wvfm_diff}
\end{minipage}
\caption{An example calorimeter waveform. The raw waveform and the resulting baseline fit (top left) as well as the smoothed waveform (top right) and the differences between samples extracted from it (bottom) are shown. Note the well visible ``oscillations'' in the difference plot, located where peaks are visible in the raw waveform.}
\label{fig:pfinder_ex}
\end{figure}


The aforementioned threshold values need to be determined somehow, as too low values cause baseline fluctuations to be detected as peaks, while too high values cause the algorithm to not recognize small, real peaks.
To obtain optimum values, the peak finding algorithm has been run on a sub-sample of the recorded data with varying thresholds.
For each tested threshold value, the total number of peaks found in the sample per channel is saved.
The number of peaks found is then plotted against the corresponding threshold value (\autoref{fig:pfinder_calib}).
How such a plot exactly looks like depends on the type of signal, but all plots have in common that, going towards smaller thresholds, the number of found peaks increases drastically after a certain threshold value.
This indicates baseline fluctuations being identified as peaks.
Consequently, threshold values are chosen such that they are not within this baseline region, while being as low as possible\footnote{This statement excludes signals with constant height, where one wants to be within a region where the number of peaks found does not change.}.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{peakfind_plot_scint}
\caption{Peak finder calibration plot for logic box channels with scintillator signals (pure and discriminated), for which both peak finder thresholds were chosen to be the same due to peak symmetry. The number of discriminator signals found converges towards a constant value for higher thresholds, while the number for non-discriminated signals decreases. This behavior is expected, as discriminator signals all have the same height, while pure scintillator signals have varying heights. The difference in signals found between the discriminated scintillator 1 signal channels in both logic boxes at thresholds below 350 ADC units are due to oscillations (ringing) occurring in box 2 after a discriminator signal.}
\label{fig:pfinder_calib}
\end{figure}

\subsection{Timing Analysis}
\label{subsec:timing_analysis}
% Author: Sebastian

The overall procedure to obtain the lifetime of the pion (and possibly that of the muon), neglecting technical details, consists of three main steps.
First, the signal of the TACs has to be converted in appropriate time units e.g. by using the calibration done in \autoref{fig:taccal}.
The obtained values are then put into a histogram such that the decay curve given in \eqref{eq:decaycurve} can be fitted to the data.
The desired lifetimes can finally be read off from the fit.

However, it turned out while analyzing the entire dataset in the way described above, that the TAC calibration of \autoref{fig:taccal} is wrong.
In fact, it does not give any reasonable time windows to be analyzed, which possibly is due to several problems with the TACs while taking data.
Therefore, another calibration had to be done, using some of the data we took with the DAQ.

\subsubsection{TAC Calibration}

In the following we will first discuss TAC1 and TAC2 separately and then compare both calibrations.

\paragraph{\textbf{TAC1}}

In order to re-calibrate the TAC values given in ADC counts one can use the signals from the scintillators 1 and 5 (cf.~\autoref{fig:setup}), i.e.~from the target and the one right in front of the calorimeter entrance window.
The difference between both signals in terms of samples that we recorded is in one-to-one correspondence to the measured time, that in turn should be proportional to the TAC1 signal measured in units of ADC counts.
Our measurement shown in \autoref{fig:timevstac1band} confirmed that this assumption is actually valid, since it shows a nice linear correlation between both quantities.

\begin{figure}[p!]
\centering
\includegraphics[width=0.7\textwidth]{timevstac1band}
\caption{Correlation between TAC1 in ADC counts and the time in samples given by the difference of the scintillator signals in 1 and 5. The two lines correspond to a shift of the TAC1 at some point of the measurement.}
\label{fig:timevstac1band}
\end{figure}

However, in the correlation figure there essentially appear two separate linear correlations, i.e.~the TAC1 must have constantly shifted at some point.
This could be due to temperature changes in the rack or accidental changes of the delays we used.

To correct for the shifting, one can do a linear fit on both correlations separately. Moreover, the runs in which the TAC shift occurred are excluded.
Thus, one can calibrate the TAC1 event by event, depending on whether the shift already took place.
The data points in \autoref{fig:timevstac1band} that were actually used for calibration can be seen in \autoref{fig:timevstac1band_calibration}.
The corresponding fit values of the linear correlation $t_{\si{spl}} = m \cdot t_{\si{ADC}} + h$ are shown in \autoref{tab:tac1_cal}.

\begin{figure}[p!]
\centering
\includegraphics[width=0.7\textwidth]{timevstac1band_calibration}
\caption{Correlation between TAC1 in ADC counts and the time in samples. The runs in which the TAC shift occurred and the low ADC counts are excluded compared to \autoref{fig:timevstac1band}.}
\label{fig:timevstac1band_calibration}
\end{figure}

\begin{table}[h]
\centering
\begin{tabular}{@{}cc@{}}
\toprule
Before shift & After shift \\ \midrule
$m = 0.01171 \pm 1 \times 10^{-6}$ & $m = 0.01163 \pm 1 \times 10^{-6}$\\
$h = -72.12 \pm 0.01$ & $h = -59.47 \pm 0.01$\\
\bottomrule
\end{tabular}
\caption{Fitted values of linear correlation $t_{\si{spl}} = m \cdot t_{\si{ADC}} + h$ to calibrate TAC1 before and after it shifted slightly, where $\left[ m \right] = \frac{\si{spl}}{\si{ADC}}$ and $\left[ h \right] = \si{spl}$.}
\label{tab:tac1_cal}
\end{table}

\paragraph{\textbf{TAC2}}

The same way we did the re-calibration of TAC1 we can proceed with TAC2 in the following.
The steps are completely similar to the above section, so we rather keep it short and just give the results of the TAC2 measurement here.

\begin{figure}[p!]
\centering
\includegraphics[width=0.7\textwidth]{timevstac2band}
\caption{Correlation between TAC2 in ADC counts and the time in samples given by the difference of the scintillator signals in 1 and 5. Only very few data points can be seen in a linear correlation.}
\label{fig:timevstac2band}
\end{figure}

In contrast to TAC1, TAC2 did not shift away during the measurement.
However, the re-calibration of TAC2 shown in \autoref{fig:timevstac2band} turns out to be rather unpromising.
Only very few data points are in the region where the linear correlation is expected, while most of them are in the region of a constant background around 1000 ADC counts, not being suitable for a linear fit.
The data points used for calibration are shown in \autoref{fig:timevstac2band_calibration}.

\begin{figure}[p!]
\centering
\includegraphics[width=0.7\textwidth]{timevstac2band_calibration}
\caption{Correlation between TAC2 in ADC counts and the time in samples after cutting away the constant background around 1000 ADC counts.}
\label{fig:timevstac2band_calibration}
\end{figure}

A linear fit of the correlation then yields (in the appropriate units, cf.~\autoref{tab:tac1_cal})
\begin{align}
m &= 0.002483 \pm 1 \times 10^{-6} \\
h &= -10.42 \pm 0.01
\label{eq:tac2_cal}
\end{align}

Since we used a sampling rate of 100 MHz for the DAQ, we can translate the samples to nanoseconds, i.e.~$1 \si{spl} = 10 \si{ns}$.
Thus, the final calibration of TAC1 and TAC2 is given in \autoref{tab:tac1tac2_cal}.

\begin{table}[h]
\centering
\begin{tabular}{@{}ccc@{}}
\toprule
	& $m$ [ns/ADC] & $h \left[ \si{ns} \right]$ \\ \midrule
TAC1 (before shift) & $0.1171 \pm 1 \times 10^{-5}$ & $-721.2 \pm 0.1$ \\
TAC1 (after shift) & $0.1163 \pm 1 \times 10^{-5}$ & $-594.7 \pm 0.1$ \\
TAC2 & $0.02483 \pm 1 \times 10^{-5} $ & $ -104.2 \pm 0.1 $ \\
\bottomrule 
\end{tabular}
\caption{Linear calibration of TAC1 and TAC2.}
\label{tab:tac1tac2_cal}
\end{table}

A simple crosscheck of this calibration is to plot the TAC1 against the TAC2 signal, which again should give a linear correlation of unit slope.
This is indeed the case as shown in \autoref{fig:tac1vstac2}, so we can use both calibrations for the following analysis of the decay curves.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{tac1vstac2}
\caption{TAC1 vs TAC2 signal. The calibration yields a reasonably linear correlation of unit slope.}
\label{fig:tac1vstac2}
\end{figure}

\subsubsection{Decay Curve Measurement}

After having calibrated both TACs correctly one can obtain the measured timing values and try to extract the expected decay curve (cf.~\eqref{eq:decaycurve} and \autoref{fig:decaycurve}).
Again, this will be done for TAC1 and TAC2 separately.
The data illustrated in the following exactly corresponds to the data used for calibration, i.e.~the data points in \autoref{fig:timevstac1band_calibration} and \autoref{fig:timevstac2band_calibration} respectively.

\paragraph{\textbf{TAC1}}

The timing measurement of TAC1 with the corresponding fit of the decay curve is shown in \autoref{fig:timetac1}.

\begin{figure}[h]
\centering
\includegraphics[width=0.99\textwidth]{timetac1}
\caption{Histogram of timing measurements of TAC1 after re-calibration. The corresponding fit of the decay curve is shown in red.}
\label{fig:timetac1}
\end{figure}

The fit yields for the respective lifetime of pion and muon
\begin{align}
\tau_\pi & = (24.24 \pm 0.46) \si{ns} \\
\tau_\mu & = (4143.0 \pm 277.3) \si{ns}
\label{eq:lifetimes_tac1}
\end{align}

However, by comparison to the decay curve predicted by theory one immediately notices the sharp excess of events in the region of the fast rising edge.
Thus, this peak has either to be excluded from the fit or to be taken into account, since it might be physical.
Both cases are shown in \autoref{fig:timetac1gauss_excl} with the respective lifetimes of pion and muon illustrated in \autoref{tab:lifetimes_gauss_excl}.

\begin{figure}[p!]
\centering
\begin{subfigure}[h]{0.99\textwidth}
\includegraphics[width=\textwidth]{timetac1_gauss}
\caption{Peak taken into account by a Gaussian distribution.}
\end{subfigure}
\begin{subfigure}[h]{0.99\textwidth}
\includegraphics[width=\textwidth]{timetac1_exclude_peak}
\caption{Peak region excluded from fit.}
\end{subfigure}
\caption{Timing measurements of TAC1. The fit of the decay curve is shown in red, comparing the cases of excluding the unexpected peak or taking it into account.}
\label{fig:timetac1gauss_excl}
\end{figure}

\begin{table}[h]
\centering
\begin{tabular}{@{}ccc@{}}
\toprule
$\tau$ & Gaussian peak & Exclude peak \\ \midrule
$\tau_\pi [\si{ns}]$ & $33.65 \pm 1.33$ & $26.71 \pm 3.32$ \\
$\tau_\mu [\si{ns}]$ & $435.7 \pm 204.0$ & $461.7 \pm 240.6$ \\
\bottomrule 
\end{tabular}
\caption{Fitted lifetimes of pion $\tau_\pi$ and muon $\tau_\mu$ for the different fit configurations.}
\label{tab:lifetimes_gauss_excl}
\end{table}

Both fits are comparable concerning the obtained values for $\tau_\pi$, $\tau_\mu$ and even the reduced $\chi^2$.
However, they seem to have a very high constant background $N_{\si{bkg}} \approx \mathcal{O}(2000)$ which is of the same order of magnitude like the number of events per bin.
Thus, the entire fit seems to be very sensitive to the fast rising edge of the spectrum.

Since it is most reasonable to exclude the peak from the fit range, this yields as a result:
\begin{align}
\tau_\pi & = (26.71 \pm 3.32) \si{ns} \\
\tau_\mu & = (461.7 \pm 240.6) \si{ns}
\label{eq:lifetimes_tac1_final}
\end{align}

Our measured value of $\tau_\pi = (26.71 \pm 3.32) \si{ns}$ agrees with the one in the literature of $\tau_\pi^{\si{lit}} \approx 26 \si{ns}$ within a 1$\sigma$ range, while the value of $\tau_\mu$ we obtained is a factor $\mathcal{O}(5)$ off from the literature $\tau_\mu^{\si{lit}} \approx 2200 \si{ns}$.
Possible reasons for these results will be discussed in \autoref{sec:discussion}.

\paragraph{\textbf{TAC2}}

In a similar way to the TAC1 procedure, one obtains the results shown in \autoref{fig:timetac2}.
The figure clearly shows that the peak in the rising edge region is even more evolved than in the TAC1 data.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{timetac2}
\caption{Histogram of timing measurements of TAC2 after re-calibration. The corresponding fit of the decay curve is shown in red.}
\label{fig:timetac2}
\end{figure}

The fit of the decay curve yields the result
\begin{align}
\tau_\pi & = (23.49 \pm 0.83) \si{ns} \\
\tau_\mu & = (9996 \pm 8260) \si{ns}
\label{eq:lifetimes_tac2}
\end{align}

Again, one can exclude the peak or take it into account by a Gaussian distribution as shown in \autoref{fig:timetac2gauss_excl}.

\begin{figure}[p!]
\centering
\begin{subfigure}[h]{0.99\textwidth}
\includegraphics[width=\textwidth]{timetac2_gauss}
\caption{Peak taken into account by a Gaussian distribution.}
\end{subfigure}
\begin{subfigure}[h]{0.99\textwidth}
\includegraphics[width=\textwidth]{timetac2_exclude_peak}
\caption{Peak region excluded from fit.}
\end{subfigure}
\caption{Timing measurements of TAC2. The fit of the decay curve is shown in red, comparing the cases of excluding the unexpected peak or taking it into account.}
\label{fig:timetac2gauss_excl}
\end{figure}

In contrast to the TAC1 measurement, there seems to be an additional structure\footnote{This peak structure might be due to the low statistics compared to the TAC1 measurement. Thus it is probably present in the TAC1 measurement as well.} in the spectrum shown in \autoref{fig:timetac2gauss_excl}.
In particular, there seem to be peaks distributed over the entire spectrum with a constant distance.
Each peak can be fitted with a Gaussian distribution on top of the ordinary fit of the decay curve and is illustrated in \autoref{fig:timetac2_gausses_fix_mu}.
Since this is a timing measurement, it turns out that the distance of the peaks can be translated into a frequency, which exactly corresponds to the beam frequency $f_{RF} = 50 \si{MHz}$, i.e.~the background structure is due to particles coming straight from the beam.
Consequently, this background is also present in the data taken by TAC1.

\begin{figure}[h]
\centering
\includegraphics[width=0.99\textwidth]{timetac2_gausses_fix_mu}
\caption{Timing measurement of TAC2. The fit of the decay curve is extended by Gaussian distributions with a constant distance to each other, which can be translated into a frequency.}
\label{fig:timetac2_gausses_fix_mu}
\end{figure}

Following the same arguments for the TAC1 analysis, by choosing the best fit one obtains for the respective lifetimes:
\begin{align}
\tau_\pi & = (25.22 \pm 3.01) \si{ns} \\
\tau_\mu & = (9334 \pm 9632) \si{ns}
\label{eq:lifetimes_tac2_excl}
\end{align}

Although the pion lifetime $\tau_\pi$ is in agreement with the value from literature in a 1$\sigma$ range, the fit does not behave well.
This can especially be seen by looking at the value for the muon lifetime $\tau_\mu$ together with its huge error.
This might be due to the low statistics in the TAC2 measurement, such that the periodic beam background gives a large contribution.


\subsection{Energy Measurement and Branching Ratios}
\label{subsec:energy_measurement}
%Authors: Nicolas, Sebastian

The procedure to obtain a branching ratio between the two decay channels \pie and \pimu consists of three main steps.
The first one is to find an appropriate integral on the calorimeter signal that has been identified as an electron  \footnote{These can be primary or secondary electrons, cf.~\autoref{sec:theory}.} from a pion decay.
As mentioned in sec. ~\autoref{subsec:lowlevel_analysis}, for every event this leads to a number proportional to the measured electron's energy.
Thus, one needs to calibrate the obtained energy spectra in a second step, i.e.~find the proportionality factor mentioned before.
In practice this is done by fitting the expected energy spectrum of the secondary electrons from \pimu (Michel spectrum) to the experimental data, as opposed to the calibration by cosmic rays explained in \autoref{subsec:setup_calo}.
Thirdly, one would like to use the fit to calculate an integral representing the total number of muon events and compare it to the total number of electron events, in order to get the ratio $R$ of the branching ratios of \pimu and \pie decay events as given in \eqref{eq:BR_ratio}.

Unfortunately, the third step could not be performed, since the expected Gaussian peak does not appear in the spectrum even with tight constraints on the particle's arrival time applied.
This will be discussed in \autoref{sec:discussion}.

\subsubsection{Total Energy of a Single Event}

Whenever the logic box got a signal from the trigger, the signal of the calorimeter in the corresponding time interval has been saved.
One finds that some of these signals have more than one peak.
This happens if two pion decays followed in a time interval that is too short to resolve.
Such signals have been dropped in the analysis.
For all single peak signals a baseline has been calculated, such that the integral of the signal of the total time interval minus the baseline integral is proportional to the energy deposited in the calorimeter.
A histogram counting up all signals in an interval leads to an uncalibrated\footnote{In the sense that appropriate energy values have still to be assigned.} energy spectrum.
Approximately 1.1 million events per calorimeter channel have been used for the following analysis.

\subsubsection{Calibration of Energy Spectra}

As was mentioned in the theory part of \autoref{subsubsec:decay_energy_spectrum} the expected energy spectrum consists of two contributions, namely the Michel spectrum, coming from the \pimu decay channel, and a sharp peak originating from the \pie decay channel.
However, in practice one does not measure a pure Michel (with the characteristic sharp edge) and sharp peak signal.
Rather the spectrum one obtains is smeared out due to the finite resolution of the detector, i.e.~in a mathematical sense the expected ideal signal is convoluted with a Gaussian distribution, accounting for the resolution.
The Idea behind the calibration was to fit this convolution $F_{\text{cmg}}$ to the measured data.
$F_{\text{cmg}}$ in appropriate units is given by
\begin{equation}
F_{\text{cmg}} = \left[ 3(\frac{x-\mu}{s})^2-2(\frac{x-\mu}{s})^3 \right] * \left[\frac{1}{\sqrt{2\pi\sigma}}e^{-\frac{x^2}{2\sigma^2}} \right]+\frac{1}{\sqrt{2\pi\sigma}}e^{-\frac{(x-70)^2}{2\sigma^2}}
\label{eq:cmg}
\end{equation}
where the first term corresponds to the convolution with the Michel spectrum and the second one to the sharp peak from \pie at $E_e = 70 \si{MeV}$.
The parameters $\mu$ and $\sigma$ denote the standard Gaussian distribution mean and deviation.

It is well known that the sharp edge of the Michel spectrum is expected at an energy of  $E_{\text{max}} = 52.8 \si{MeV}$ (cf.~\autoref{sec:theory}).
Thus, using the fitting parameters one obtains the exact position of the edge and the resolution of the detector.
In particular, with an appropriate scaling of $E_{\text{max}}$ with respect to the Michel edge, $\sigma$ is exactly the resolution of the calorimeter.
The corresponding fit yields values for the resolution around 4 MeV as shown in \autoref{tab:calo_resolution} and therefore approximately  $\mathcal{O}(10)$ MeV better than predicted by the cosmic rays calibration\footnote{The energy calibration via cosmic rays was already anticipated to be very imprecise in \autoref{subsec:setup_calo}.}.

\begin{table}[h]
\centering
\begin{tabular}{@{}ccc@{}}
\toprule
Channel & $R_\text{Michel}$ [MeV] & $R_\text{cosmic}$ [MeV] \\ \midrule
    0 & $4.57\pm 0.1$ & $16.5$\\
    1 & $5.5 \pm 0.2$ & $16.3$\\
    2 & $2.7 \pm 0.05$ & $13.9$\\
    3 & $4.4 \pm 0.1$ &  $18.0$\\
    4 & $4.4 \pm 0.2$ &  $17.9$\\
    5 & $4.2 \pm 0.1$ &  $13.5$\\
    6 & $4.1 \pm 0.1$ &  $17.3$\\
\bottomrule
\end{tabular}
\caption{Resolution $R$ of each calorimeter channel determined by mean square fit of the Michel spectrum ($R_\text{Michel}$) and by the cosmic ray analysis ($R_\text{cosmic}$).}
\label{tab:calo_resolution}
\end{table}

However, the fit of the sum of both contributions as defined in \eqref{eq:cmg} did not converge properly, so we just fitted the first term of it, i.e.~the Michel spectrum convoluted with a Gaussian distribution.
As one can see for calorimeter channel 0 in \autoref{fig:Calo0Calibrated} this behaves and converges well.

\begin{figure}[h]
\centering
\includegraphics[width=0.99\textwidth]{Calo0Calibrated}
\caption{Uncalibrated histogram of the energy spectrum of calorimeter channel 0 with a bin width of 0.36 MeV, showing 1.2 million events. Fitting range is between 24 MeV and 53 MeV. The Michel spectrum convolution function,used as calibration function, is shown in red.}
\label{fig:Calo0Calibrated}
\end{figure}

One would expect a peak around $E_e = (70 \pm 2.5) \si{MeV}$, but on the full set of data there is no visible peak that could have been fitted by an unbinned maximum likelihood method or by a mean squared method.
Unfortunately, this is the case for all seven channels of the calorimeter.

However, a distinct peak can be observed in some channels when applying a constraint on the calibrated timing.
Since the pion lifetime $\tau_\pi$ is much shorter than the one of the muon $\tau_\mu$, i.e.~$\tau_\pi \ll \tau_\mu$, we expect most of the primary electrons from \pie to be measured at early times in the timing distribution of e.g.~\autoref{fig:decaycurve}.
Thus, demanding the electron's time to lie in between -85 ns to -70 ns, so in the region one would expect, a narrow peak can be observed around 65 MeV shown in \autoref{fig:Calo0TimeConstraint}.

\begin{figure}[h]
\centering
\includegraphics[width=0.99\textwidth]{timevsenergy}
\caption{2D Histogram showing the energy versus time distribution. Be aware of the smeared line on the very left and the noise over 50 MeV over all times.}
\label{fig:EnergyVsTime}
\end{figure}

\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.99\textwidth}
\includegraphics[width=\textwidth]{energyprojectionCalo0_85}
\caption{Histogram of the energy, using a time constraint $t \in \left[ -85 \si{ns}, -70 \si{ns} \right]$. A slight peak can be seen at 65 MeV.}
\label{fig:Calo0TimeConstraint}
\end{subfigure}
\begin{subfigure}[t]{0.99\textwidth}
\includegraphics[width=\textwidth]{energyprojectionCalo0_100}
\caption{Histogram of the energy, using a time constraint $t \in \left[ -100 \si{ns}, 0 \si{ns} \right]$.}
\label{fig:Calo0TimeConstraint100}
\end{subfigure}
\caption{Energy spectrum with time constraints for calorimeter channel 0.}
\label{fig:Calo0TimeConstraints}
\end{figure}

Unfortunately, this excess cannot correspond to the primary electrons, because of several reasons.
The most striking argument is the obvious energy mismatch.
By theory considerations we would expect the electron energy to be $E_e \approx 70 \si{MeV}$, which is clearly not what we observe in this short time window.
Furthermore, the excess of data is only visible in this very specific timing window $t \in \left[-85 \si{ns}, -70 \si{ns} \right]$.
When loosening the constraint to times $t \in \left[ -100 \si{ns}, 0 \si{ns} \right]$, the peak disappears as shown in \autoref{fig:Calo0TimeConstraint100}.
Another argument can be made, taking the energy resolution ($\mathcal{O}(4) \si{MeV}$) of the calorimeter into account.
The observed peak is too narrow to be resolved by the calorimeter we used.
Thus, we can conclude that this peak does not correspond to primary electrons from the \pie decay.
A reasonable suggestion for this fluctuation would be the high energy background originating from the beamline, which we already encountered in the timing analysis\footnote{This will be made more precise in \autoref{sec:discussion}.}.

It has also been tried to find a peak on the full dataset summing up all seven calibrated calorimeter channels as shown in \autoref{fig:calosum}.
What can be observed here is something like a peak that is smeared out at $E \approx 70 \si{MeV}$ which might also be due to the background coming from the beam, that we already encountered in the timing measurements (cf.~\autoref{fig:timetac2_gausses_fix_mu}).
However, its width unfortunately grows as one uses more data.
Therefore, we conclude that this peak cannot be distinguished from a statistical fluctuation.

\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.99\textwidth}
\includegraphics[width=\textwidth]{CaloCalibrated1Plot}
\caption{Calibrated calorimeter spectrum for each of the seven channels.}
\label{fig:calocal1plot}
\end{subfigure}
\begin{subfigure}[t]{0.99\textwidth}
\includegraphics[width=\textwidth]{CaloSum}
\caption{Sum of all calibrated calorimeter channels.}
\label{fig:calosum}
\end{subfigure}
\caption{Calibrated calorimeter energy spectra.}
\end{figure}
