import matplotlib.pyplot as plt
plt.plot([20,21,22,23,24,25,26,27,25.5,25.5,24.5,23.5,24.75,24.25], [120,115,150,430,1200,1300,480,100,900,900,1400,780,1400,1340], 'bo')
plt.axis([19, 29, 0, 1600])
plt.title('Absorber Thickness vs pi-stop signal')
plt.xlabel('Absorber Thickness (cm)')
plt.ylabel('Rate (Hz)')
plt.savefig("rangekurve", ext="png", close=False, verbose=True)
#plt.show()




