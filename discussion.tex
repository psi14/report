\section{Discussion}
\label{sec:discussion}
This section is separated into three parts.
In the first one, we shall discuss the results on finding the pion and muon lifetime.
In the second one we shall discuss the difficulties in finding the branching ratios of the decay channels \pie and \pimu.
In the third part we shall finally give a brief summary of the measurement.

\subsection{Timing}
\label{subsec:disc_timing}

In \autoref{subsec:timing_analysis} the calculation of the pion and muon lifetime, $\tau_\pi$ and $\tau_\mu$, was shown by fitting the respective decay curves shown in \autoref{fig:timetac1gauss_excl} and \autoref{fig:timetac1gauss_excl}.
The values obtained by the fits can be seen in \autoref{tab:lifetimes_final}.

\begin{table}[h]
\centering
\begin{tabular}{@{}ccc@{}}
\toprule
$\tau$ & TAC1 & TAC2 \\ \midrule
$\tau_\pi [\si{ns}]$ & $26.71 \pm 3.32$ & $25.22 \pm 3.01$ \\
$\tau_\mu [\si{ns}]$ & $461.7 \pm 240.6$ & $9334 \pm 9632$ \\
\bottomrule 
\end{tabular}
\caption{Fitted lifetimes of pion $\tau_\pi$ and muon $\tau_\mu$ for the TAC1 and TAC2 measurement.}
\label{tab:lifetimes_final}
\end{table}

For both measurements the measured value of $\tau_\pi$ was in reasonable agreement with the theoretical expectation (cf.~\autoref{fig:decaycurve}).
However $\tau_\mu$ has been obtained with strong disagreement compared to the expected value.
The reason for this is that the fit is very sensitive to the flat plateau of long times that it has to take into account.
Depending on the constant offset $N_{\si{bkg}}$ it yields almost every value of $\mathcal{O}(10^3) \si{ns}$.
However, a constant offset would correspond to a constant background signal of the TAC, which we assume to be as low as possible.
Thus, the muon lifetime $\tau_\mu$ is wrong due to pileup\footnote{The measurement has to be done differently to actually measure $\tau_\mu$.}.

Another obvious difference when comparing the measured decay curve with the theoretical expectation is an excess of events in the region of the fast rising edge, where we expect the \pie decays.
As was indicated in \autoref{subsec:timing_analysis} this might be due to background originating from the beam, which is difficult to correct for, since we only see the small excess on top of the timing measurement.
This becomes evident in the measurement of TAC2 due to the low statistics.
In another experiment one could take care for this by e.g.~using an additional or more efficient veto scintillator.

If we were able to correct for this background, the value of \pie would probably have been even closer to expectation.


\subsection{Branching Ratios}
\label{subsec:disc_branching_ratio}
%Authors: Nicolas, Sebastian

In \autoref{subsec:energy_measurement} we saw that a good calibration of the calorimeter can be achieved.
Also one obtains a reasonable value for the energy resolution of every calorimeter.
However, the issue remains that no statistically relevant peak at $E_e = 70 \si{MeV}$ could be observed, using the full dataset.

In order to calculate a branching ratio of the \pie and \pimu decay channels one needs to be able to determine a reasonable number of \pie events, but unfortunately we cannot distinguish the real \pie events from high-energy background.
The precise physical reason for this background we do not know, but an obvious suggestion would be the high energy particles originating from the beam as seen in the timing measurement of TAC2.

When repeating the experiment one would need to take care of this possible background sources, since in the analysis it is not possible to get rid of it anymore\footnote{Except for cases where one has a reasonable physical model to reproduce the background, such that it can be subtracted from the data.}.
Another possibility to look for the \pie peak with more precision is to use a calorimeter with a better energy resolution.
This would lead to sharper peaks and it would be easier to distinguish between peak and background.
On the dataset with timing constraint we do see a peak in the expected region, but it can be concluded that it does not correspond to electrons from the pion decay.
Therefore, given the data and insufficient knowledge about the background it is not possible to calculate any reasonable branching ratio.


\subsection{Summary}
\label{subsec:outlook}
%Authors: Sebastian

This report is dedicated to give an introduction and general overview on a measurement of lifetimes and branching fractions in pion decays performed during the PSI lab course 2014, including chapters on electroweak theory, measurement methods and data analysis.
In particular, we measured the lifetime $\tau_\pi$ of the $\pi^+$ meson and branching ratios of its weak decay channels \pie and \pimu.

Combining the measurements of both TACs, the analysis of the $\tau_\pi$ measurement resulted in values of
\begin{equation*}
\tau_\pi = (25.79 \pm 3.17) \si{ns}  \mathend ,
\end{equation*}
i.e.~on the one hand $\tau_\pi$ reasonably agrees with the value in the literature of $\tau_\pi^{\si{lit}} \approx 26 \si{ns}$ within a 1$\sigma$ range, which is a remarkable result in its own right.
On the other hand, however, the lifetime of the muon $\tau_\mu$ is in strong disagreement with the value in the literature $\tau_\mu^{\si{lit}} \approx 2200 \si{ns}$, which was probably due to pileup.
Furthermore, an excess of high-energetic background caused by particles originating from the beam was observed in the timing distribution during our measurement, being also visible in the energy measurement.

The original idea in order to calculate the different branching fractions of \pie and \pimu was to do an energy measurement of the decay electrons in a calorimeter, resulting in Michel spectra of secondary electrons due to Michel decays \mue and hopefully the sharp peak of the \pie decay expected by electroweak theory predictions.
The Michel part of the spectrum was used to calibrate the PMTs attached to the calorimeter with respect to energy and to determine its resolution.
However, despite the fact that the Michel spectra we obtained are in reasonable agreement with electroweak theory predictions, we were not able to observe the \pie decays in a statistically relevant way -- the expected peak could not be observed in the energy spectrum of the entire set of data.
This seems again to be due to some high energy background\footnote{Corresponding to the peak background in the timing distributions.}.
The most likely explanation might be the insufficient efficiency of the veto scintillator we used, so that some high energy particles from the beam were scattered straight into the calorimeter without being detected as background by the veto.
Another difficulty in finding the peak was the energy resolution of the calorimeter, i.e.~the expected peak is definitely smeared out, making it even harder to distinguish it from the high energy background we observed.
Thus, we were not able to determine the respective branching ratios of \pie and \pimu.

In summary, it can be stated that we succeeded in part of our measurement goals, i.e.~in determining the pion's lifetime to be $\tau_\pi = (25.79 \pm 3.17) \si{ns}$, whereas we encountered unexpected difficulties in measuring the different branching rations of \pie and \pimu, such that no quantitative result could be achieved.
