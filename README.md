# README #

In the Source page of this repository you can find the file `PSIstyle.C`. 

This script defines the style of the plots we are going to include in our Report of the PSI Experience 2014.

This README contains some instructions on how to use the style script for our plots.

**Notice:** this is still the first version of the script, it needs to be tested and improved, so comments and remarks are very welcome. 

### How to use it ###

Put this file in the same folder you will run the scripts which will make your plot(s). Include this file in your script as follows:
```
#include "PSIstyle.C"
```
then call the function `PSIstyle()` inside the one which will make your plot(s).

### Content and structure ###

Until now, the script content is very simple. Inside the `void` function, a [TStyle](https://root.cern.ch/root/html/TStyle.html) is defined as follows: 

```
TStyle* PSIstyle = new  TStyle("PSIstyle", "Style for our plots")
```
What comes after is a list of general settings for our plots, whether they be histograms or functions or graphics objects. It is possible to read (almost) exactly what the different lines, or groups of code lines, do in my comments in the code itself. An example is:

```
/***turn off plot infos***/
PSIstyle->SetOptStat(0); 
// PSIstyle->SetOptStat(1111);  // if you want to show #entries, mean, rms, name of the histogram - for more options ---> TStyle::SetOptStat
PSIstyle->SetOptFit(0); 
// PSIstyle->SetOptFit(1011); // if you want to show probability, parameters and errors - for more options ---> TStyle::SetOptFit
PSIstyle->SetOptTitle(0); // 1 for the title to appear on your plot
PSIstyle->SetOptDate(0);
```

In the code above you can see how to show (or not) in your plot all (or some) data about your histogram or fit, and how to show or not your plot's title and date. In this case, the default ROOT statistics box showing data about your histogram is switched off with `PSIstyle->SetOptStat(0)`, but in case you want it to appear you can simply comment this line and uncomment the next one ` PSIstyle->SetOptStat(1111)` for your statistics box to appear and show the number of entries, the mean, the root mean square and the name of your histogram, according to the given options `(1111)`. If you need more options, sometimes I also specify the ROOT class you should look for in order to find them (as in this case ` for more options ---> TStyle::SetOptStat`).
For lines as `PSIstyle->SetOptTitle(0)` instead the possible options are often just `1` or `0`, which mean `on` and `off` respectively. 

Out of the `void` some `TObjects` such as TPaveText or TLegend are defined. These objects may be used in your plots when necessary, that is you may use a TLegend to be filled with the wanted entries instead of the default (and ugly) statistics box. Here is then an example of code and what you should do to use one of these objects:

```
TLegend *PSILegend = new TLegend(0.65,0.7,0.88,0.88,"Legend Title (supports LaTeX)","BRNDC")
```
this defines the "object legend" called `PSILegend`. This line is commented by default in the style script, since the initial plot is expected to be *clean*. Nevertheless, if you need to draw this object, uncomment the line which defines it and go to check the lines of code in which its settings are defined. In our case:

```
/*
//TLegend *PSILegend = new TLegend(140,6000,160,8200,"Legend Title (supports LaTeX)","BR");
//
PSILegend->SetTextFont(PSIFont);
PSILegend->SetTextSize(0.025);
PSILegend->SetTextColor(1);
PSILegend->SetTextAlign(12);
PSILegend->SetBorderSize(0);
//PSIstyle->SetLegendBorderSize(0);
//PSILegend->AddEntry(TObject,"legend entry","lepf");
//PSILegend->Draw();
*/
```

The entire block is commented for the same reason I wrote before, that is the initial plot is supposed to be clean. But once you need to draw this object, you just need to cancel the initial `/*` and final `*/` for the block to be uncommented. The lines 
```
//TLegend *PSILegend = new TLegend(140,6000,160,8200,"Legend Title (supports LaTeX)","BR");
//
//PSIstyle->SetLegendBorderSize(0);
//PSILegend->AddEntry(TObject,"legend entry","lepf");
//PSILegend->Draw();
```
must stay commented because they are not part of the code and they are there (the last three ones) just to illustrate what can you write in your code to define further setting for this objects and draw it.

The remaining lines
```
PSILegend->SetTextFont(PSIFont);
PSILegend->SetTextSize(0.025);
PSILegend->SetTextColor(1);
PSILegend->SetTextAlign(12);
PSILegend->SetBorderSize(0);
```
set the style of the legend according to (hopefully) the style of the whole plot. 

### What I suggest ###

* A general suggestion is to not uncomment anything unless I say in my comments you can (or should) do it or you are uncommenting an entire block (e.g. removing `/* - */`).
* A ROOT default statistics or fit data box is ugly. Don't use it and use paves or legends you can give the entries to and you can arrange in order for them to look, if not beautiful, at least less ugly.
* If you think this README is too stupid or it hurts your intelligence please tell me, I do not want to offend anyone with it.
* If you think this README is kind of useful but it needs to be clearer or written in a more convenient way, again, please tell me.
* If there is any obscure part please ask, and please point out any possible mistake.
* Whenever you need the script to do something specific which can be useful for everyone, please tell me and I will add it.

### Who to blame...I mean...ask if nothing works? ###

Until now: only Dorothea II.

You have my email, skype, facebook, phone number and home address. The last one only few of you (luckily).