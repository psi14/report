\section{Experimental Setup}
\label{sec:setup}

\subsection{Measurement Methods}
%Author:Michael%%

To measure the pion lifetime, a measurement of the time difference between the pion arrival in the target and the corresponding signal of the decay-electron, entering the calorimeter is needed.
Furthermore, a measurement of the energies of the electrons in both decay channels is needed in order to obtain the branching fractions of the pion decay into electrons and muons.
That being said, our setup needs to be able to separate pions from the other beam components, to measure the timing of the decay electrons and to distinguish between the direct decay into an electron and the much more likely decay via a muon. \newline

An excellent tool to track and do timing measurements of charged particles are scintillators.
A particle traversing the scintillating material will deposit some of its energy in the material, where the energy is transformed into light, being fed into a lightguide and sent to a photosensitive device for the readout.
In our case, photo multiplier tubes (PMTs) have been used for this last step.
In our setup organic (plastic) as well as inorganic (NaI(Tl)) scintillators are used.
In general, plastic scintillators are easy to produce in various shapes and usually have a faster response, which is the reason why they have been employed in time measurements, while inorganic scintillators are expected to give a better signal for an energy measurement and are therefore used in the calorimeter. \newline

The different parts of our experimental setup will be explained in detail in the following subsections to finally give an overview of the entire measurement.


\subsection{Beam}
\label{subsec:Beam}
% Author: Dorothea II
% Comment: first draft, to be improved soon, any suggestion is welcome 

% \item[Beam] \hfill \\ The primary $590 \si{MeV}$ proton beam comes from the HIPA accelerator at the Paul Scherrer Institute. It collides against a carbon target, which leads to a secondary $\pi^+$ beam of $161 \si{GeV}$ . More details about the beam are given in Sec. \ref{sec:Beam}.

To perform the experiment, a secondary pion beam line provided by the PSI High Intensity Proton Accelerator (HIPA) Facility has been used.

The main proton beam acceleration process consists of three main steps as listed below.
\begin{enumerate}[(i)]
  \item At the first stage, protons are extracted from a hydrogen source and guided through the pre-acceleration stage by a Cockroft-Walton accelerator which also contains the proton source.
  \item The pre-acceleration stage is achieved in a small ring cyclotron named Injector II, where protons reach a kinetic energy of about 72 MeV.
  \item The last acceleration process that the proton beam undergoes before being sent to several secondary beam lines, is realized when the proton beam is fed into the center of the High Intensity Ring Cyclotron.
  Here, according to the general working principle of a ring cyclotron, protons are constrained by eight (in our specific case) bending magnets to travel a spiral orbit, being gradually accelerated by four RF cavities.
  The High Intensity Ring Cyclotron is the core of the PSI proton accelerator facility. It has 15 meters in diameter in which protons are accelerated in 186 revolutions up to a kinetic energy of 590 MeV.
 \end{enumerate}
Once the proton beam has been ejected from the 590 MeV Ring Cyclotron, it is sent to two carbon targets, namely Target E (60 mm thickness) and Target M (4 mm thickness) with the help of a system of bending dipole and focusing quadrupole magnets.
A concrete and iron shielding of the beam line prevents the diffusion of ionizing radiation through the whole experimental hall.

At our site, the pion beam has been provided by the $\pi$M1 line (cf.~\autoref{fig:M1}) coming from the Target M, and it has been prepared according to the following settings:
\begin{enumerate}[(a)]
 \item $\pi^+$ momentum of $p_\pi = 161 \si{MeV}$.
 \item RF of the HIPA  $f_{RF} = 50 \si{MHz}$.
\end{enumerate}
Other settings such as the aperture of the slits have been arranged during the beam testing period accordingly to our special need, leading to a final beam rate of about 4.3 MHz.
As already mentioned, the pion beam is partly contaminated by the presence of muons and electrons.
The $\pi^+$ beam energy is then chosen to optimize the time of flight separation between the $\pi^+$, $\mu^+$ and $e^+$ at scintillator 7, which is placed $25 \si{m}$ away from the Target M.

\begin{figure}
\centering
\includegraphics[scale = 0.12]{piM1_line}
\caption{Schematics of the $\pi$M1 line coming from Target M.}
\label{fig:M1}
\end{figure}

%To be added: target reaction for pion production, pion flux plot, what else?

\subsection{Range Curve}
% \item[Range Curve] \hfill \\

The idea of the experimental setup is such that the pions are stopped inside the active target.
To accomplish this, several layers of plastic absorber material were inserted in the beam line, in order to shed off the pion's energy before they hit the target.
The amount of absorber material required to stop the pions inside the target has been estimated according to the so called \textit{range curve} shown in \autoref{fig:rangecurve}, in which the absorber thickness is plotted against the number of $\pi$-stop signal events.

The absorber thickness corresponding to the maximum number of these events can be easily read off from the plot and resulted to be $24.5 \si{cm}$ in the present case.

In order to get a rough estimate for the needed length of absorber material, the range of a pion in lead \cite{PDG} was considered. 
The calculated range is $13 \si{cm}$ which is a factor of two smaller than the absorber length used in the experiment.

\begin{figure}[h]
\centering
\includegraphics[width=0.75\textwidth]{rangecurve}
\caption{The absorber thickness versus $\pi$-stop signal rate. The appropriate amount of absorber material to be used is $24.5 \si{cm}$.}
\label{fig:rangecurve}
\end{figure}

\subsection{Calorimeter}
\label{subsec:setup_calo}
%Authors: Colton+Gael

% \item[Calorimeter] \hfill \\
% The energy of the decay electrons is measured in the calorimeter. The energy spectrum shown in figure \ref{fig:energyspec-e} is expected.

A calorimeter\footnote{A more complete description of the calorimeter used can be found in \cite{cal}.} is used to measure the energy of the scattered particles and is therefore a powerful tool to distinguish between the \pimu and the \pie decays (cf.~\autoref{fig:energyspec-e}).
It consists of two NaI(Tl) crystals \footnote{$ \rho = 3.69 \si{\frac{g}{cm^3}}$, $-\frac{\mathrm{d}E^*}{\mathrm{d}x} = 4.8 \si{MeV/cm}$, $\tau_{\text{decay}} = 245 \si{ns}$, $LY =  38 \frac{\gamma}{\si{KeV}}$} optically coupled together and to seven PMTs, as illustrated in \autoref{fig:cal}.
The front is shielded by a lead disk with an entrance window at its center to make sure that only events from the $e^+$-\textit{branch} are registered and background emanating from the beam itself is minimized.
The scintillating NaI(Tl) crystals are surrounded by lead.

\begin{figure}[h]
\centering
\includegraphics[scale = 0.17]{calo}
\caption{Sketch of the calorimeter. The $e^+$ are entering the calorimeter from the left.}
\label{fig:cal}
\end{figure} 

The incoming positron generates an electromagnetic shower in the crystal and the produced particles cause a scintillation in the material.
The scintillation emits photons again, which are collected by the PMTs attached.
In detail a single photon arriving at the photo cathode tube produces a scattered electron via the photoelectric effect.
This signal is amplified by the production of an avalanche of secondary electrons whose arrival in the final cathode leads to a pulse in the capacitor.
After integration of the pulse, the number of secondary electrons is obtained, which is proportional to the number of photons and therefore to the energy of the stopped $e^+$.

%%I recommend we add the calibration of the Calorimeter to this section - Colton%%

With the help of an oscilloscope and using cosmic rays, each of the seven PMTs can be calibrated to a normalized signal height, adjusting the high voltage supplied of each one.
By doing so, this removes a systematic error when comparing signal heights.
%%%Did we actually use the cosmic ray data we gathered to calibrate the calorimter?%%%



%Colton's part. I merged it together with my report. Gael

%%The electron calorimeter consist of two inorganic NaI(Tl) ($ \rho = 3.69 [\mathrm{\frac{g}{cm^3}}] $) crystals optically coupled together and to seven photomultiplier tubes (PMT). The front is covered by a disk that prevents background caused by the beam going directly into the calorimeter. The NaI crystal are surrounded by 10 $\mathrm{cm}$ of lead.%%Not sure it is 10cm. Gael 
%%
%%The calorimeter is used to measure the energy of the scattered particles and is therefore a powerful tool to distinguish between the $\mu$ from the $\pi ^+$ decay. The incoming positron generates a shower in the crystal and the light falling on the photon-cathode tube produce a scattered electron by photoelectric effect. The signal is amplified by the production of secondary electrons whose arrival in the final cathode leads to a pulse in the capacitor. After integration of the pulse, the number of secondary electrons is obtained, which is proportional to the number of photons and therefore to the energy of the stopped $e^+$.

%%I recommend we add the calibration of the Calorimeter to this section - Colton%%

%%With the help of an oscilliscope, each of the seven PMT's were calibrated to a normalized signal height, produced by cosmic rays, by changing the high voltage supplied to each one. In doing so, this removes the systematic error when comparing signal heights.
%%%Did we actually use the cosmic ray data we gathered to calibrate the calorimter?%%%

\subsubsection*{Resolution and Calibration}

Assuming the calorimeter response is linear with respect to the energy of the incoming particle, the calibration requires two parameters.
Since zero energy measured corresponds to the absence of a signal, only one scaling parameter $r$ \footnote{in units of $\si{MeV/Vns}$.}, defined by
\begin{equation}
E_{\text{true}} = r E_{\text{cal}}
\end{equation}
is needed, where $E_{\text{true}}$ is the energy of the particle in MeV and $E_{cal}$ the integrated signal of the PMTs (in units of $\si{Vns}$).

% Which timing? Do we need a sketch of the trigger logic here or in \subsection{Scintillators} ??
%This section should have a detailed explanation about the TAC and the delay. We could also include the trick we used be setting the pi-stopp as the stop of the TAC by delaying it. I moved the following text in the "overview" section. I think it would be convenient for the reader to have a short summary about how we plan to measure things before going into the details. I put both the setup and logic figure there in the beginning. Gaël
As already mentioned, $r$ can be determined making use of cosmic rays.
Most of them are muons at sea level with an average energy of $E_{\mu} = 4 \si{GeV}$ \cite{PDG}.
This value is close to the muon minimum ionizing energy $E_{\text{min}} = 0.3 \si{GeV}$ \cite{PDG}.
One can approximately estimate the mean energy loss rate $\left(-\frac{\mathrm{d}E}{\mathrm{d}x}\right)$ by evaluating it at $E_{\mu} = E_{\text{min}}$, since it only increases logarithmically with $E_{\mu}$.
For a NaI(Tl) crystal this yields a value of $4.8 \si{MeV/cm}$ \cite{PDG}.



%\begin{figure}[h]
%\centering
%\begin{subfigure}[t]{0.45\textwidth}
%\includegraphics[width=\textwidth]{scop_PMT4}
%\caption{Energy distribution of the cosmic rays observed in the scope for the PMT 4. The peak on the left comes from the baseline, where no particles are detected. The offset of the right peak allows us to compute the scaling parameter $r$ for each PMT and to perform the calibration. The width can be used to determine the resolution of the calorimeter.}
%\label{fig:pmt4_scope}
%\end{subfigure}
%\begin{subfigure}[t]{0.45\textwidth}
%\includegraphics[width=\textwidth]{pmt4_fit}
%\caption{Fit of the cosmic energy spectra of PMT 4. The fitting function is a double width Gaussian.}
%\label{fig:resolution}
%\end{subfigure}
%\caption{Energy distribution and fit of cosmic ray spectra of PMT 4.}
%\label{fig:pmt_fit}
%\end{figure}
\begin{figure}[h]
\centering
\includegraphics[width=0.75\textwidth]{pmt4_fit}
\caption{Energy distribution and fit of cosmic ray spectra of PMT 4. The fitting function is a double width Gaussian. The peak on the left is an artifact of the baseline, where no particles are detected. The offset of the right peak allows us to compute the scaling parameter $r$ for each PMT and to perform the calibration. The width can be used to determine the resolution of the calorimeter.}
\label{fig:pmt4_fit}
\end{figure}

\begin{figure}[th]
\centering
\begin{subfigure}[t]{0.49\textwidth}
\includegraphics[width=\textwidth]{calo_cosmic_cal}
\caption{Calibration factor $r$ for the seven PMTs. The errors of the $\chi^2$ estimate for the position of the peak have been used. The uncertainty on the position of the baseline have not been accounted for.}
\label{fig:pmt_scaling}
\end{subfigure}
\begin{subfigure}[t]{0.49\textwidth}
\includegraphics[width=\textwidth]{calo_cosmic_res}
\caption{Resolution $\sigma$ for the seven PMTs in $\si{MeV}$. The error shown only corresponds to the Gaussian fit error of the cosmic ray peak, scaled by the factor $r$. The systematic error done by the energy deposit approximation is in fact much larger.}
\label{fig:pmt_resolution}
\end{subfigure}
\caption{Calibration factor and resolution for the seven different PMTs.}
\label{fig:pmt_scal_and_reso}
\end{figure}



To perform the calibration, the calorimeter is sandwiched vertically  between two scintillators and triggered  by the coincidence of these scintillators.
Cosmic rays passing through both the scintillators and the calorimeter are detected and their energy deposition in the calorimeter is measured, leading to the corresponding spectrum shown in \autoref{fig:pmt4_fit}.
The expected energy deposit is given by
\begin{equation}
\Delta E = -\frac{\mathrm{d}E}{\mathrm{d}x}\left(E_{\text{min}}\right)\Delta x
\end{equation}
where $\Delta x$ is the distance traveled by the cosmic muons in the NaI(Tl) crystal.
Taking $\Delta x$ as the diameter of the calorimeter, yields $\Delta E = 127 \si{MeV}.$
The position of the peak $x_p$ in \autoref{fig:pmt4_fit} should therefore correspond to $127 \si{MeV}$, which leads to
\begin{equation}
r = \frac{127 \si{MeV}}{x_{p}}.
\end{equation}
Using the width of the energy spectra, one can also deduce the resolution of the calorimeter.

To estimate the width and the position of the peak, a $\chi^2$ fit of the energy spectra with a Gaussian distribution with two widths is used, as shown in \autoref{fig:pmt4_fit}.
The range of  $\chi^2/\text{ndf}$ for all PMTs is about $0.654-1.931$.
The result for the resolution and scaling factor for the seven scintillators are displayed in \autoref{fig:pmt_scal_and_reso}.

However, using cosmic rays is not the most precise way to calibrate the PMTs.
Since the size of the internal structure of the calorimeter is not known, setting the height as the muon path length in the NaI(Tl) is only a rough approximation and may introduce a significant systematic error.
Furthermore, the energy distribution of the cosmic rays, which all the calibrations rely on, has to be known very accurately.

An alternative to this calibration procedure would be the use of the position of the falling edge in Michel spectra (cf.~\autoref{fig:energyspec-e}), which is situated at the energy of $E_e = \frac{m_\mu}{2}$ and can provide a way to inter-calibrate the PMTs.
Moreover, by convoluting it with a Gaussian, an estimation of the calorimeter's resolution can be performed. 

% \subsubsection{Resolution and Calibration}
%Author: Gael


\subsection{Scintillators}

As they are one of the primary sources for information, the selection of scintillators is integral for acquiring reasonable data.
Already mentioned in the previous section, the size of the scintillator was one of the most important properties determining where it would be used in the overall experimental setup.
Naturally, the scintillators forming the beam coincidence should cover the beam-slit, while the vetoes should cover enough area to be effective.
One of the sizing limitations, however, arises from the calorimeter setup, where the scintillator placed immediately in front of the calorimeter does not completely cover the entrance window.
During the setup we were up to the option of using a larger or smaller scintillator in relation to the calorimeter entrance window.
Although using the smaller scintillator leads to a decreased trigger rate, relative to when using the full area, this prevents a lot of zero signals produced by the calorimeter.

For the entire setup to work properly, signal sizes and efficiencies of the different scintillators are also relevant.
In general the scintillators produced well defined signals above the noise level, however in some cases a compromise between optimum efficiency and signal-to-noise ratio needed to be made.
This will result in a higher statistical uncertainty (fewer events), but should compensate for the systematic uncertainty by giving more well-defined peaks.
Critically, there was little compromise for the target scintillator, which consistently produced large signals operating at near-peak efficiency.
Moreover, the veto scintillators were operated at maximum efficiency, with less concern about signal size.

Another limitation of the scintillators originates from their operating voltages.
As was just mentioned, the scintillators were operated at their most effective voltages, which required a flexible high voltage supply to deliver widely varying voltages.
The voltage supply, however, could not deliver more than 2 mA through one channel (to one scintillator), which ultimately limited the beam operation parameters.
Upon initial testing, all scintillators functioned, but when increasing the pion momentum above 161 MeV the drawn current in select scintillators exceeded 2 mA.
This placed a practical limitation on the operation flexibility.

% \subsubsection{Coincidence Measurements}
% Author: Sotiris


\subsubsection{Efficiency in Coincidence Measurements}
As described earlier, a signal pulse is usually produced by multiple photoelectrons emitted from the photocathode of the PMT, which creates a higher pulse than dark current does.
Therefore, one uses a discriminator to effectively eliminate most dark current pulses with lower amplitudes.
However noise pulses with higher amplitudes may be caused by radiation released from natural radioactive sources or coming from the atmosphere like cosmic rays.
In that case we use the coincidence measurement setup by which we try to count the simultaneous occurrence of two or more events -- depending on the number of scintillators we use -- emitted from a radioactive source $^{90} \si{Sr}$ within a finite time.
Therefore, we can eliminate the possibility of accidental coincidences, which can occur from uncorrelated events coming from background radiation apart from our source $^{90} \si{Sr}$.
 
\subsubsection{Coincidence Setups}
Two scintillators were used for the coincidence measurements.
To accomplish this step we put the two scintillators on top of each other and connected the photomultipliers to the high voltage unit as shown in \autoref{fig:scint_12}.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{Scintillators12}
\caption{Coincidence setup of two scintillators.}
\label{fig:scint_12}
\end{figure}

After this step we placed another scintillator between scintillators 1 and 2 and tried again to achieve coincidence between them as shown in \autoref{fig:scint_123}.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{Scintillators123}
\caption{Coincidence setup of three scintillators.}
\label{fig:scint_123}
\end{figure}

Following these two steps we can measure the efficiency $\epsilon_3$ of scintillator 3 placed in the middle.
It is given by
\begin{equation}
\epsilon_3 = \frac{N_c^{13}}{N_c^{12}}
\end{equation}
where $N_c^{ij}$ is the number of coincidence pulses in the combination of scintillators $i$ and $j$.
%\begin{displaymath}
%efficiency = \frac{\sum_ {scintillator 1}^{scintillator 3} coincidence pulses}{\sum_{scintillator 1}^{scintillator 2} coincidence  pulses}
%\end{displaymath}\\

\subsubsection{Plateau Characteristics}

The next step is to check how the counting rate of the scintillators is influenced by noise while changing its voltage.
This can be done by means of a plateau curve, i.e.~we measure the plateau characteristics by setting a discrimination level and counting all pulses with amplitudes higher than this level while changing the supply voltage of the attached PMT.
We increased the PMT supply voltage while the discrimination level is kept constant, plotting the count rate as a function of the voltage yielding the corresponding plateau characteristic, representatively shown in \autoref{fig:plateau_curve} for scintillator 1.

\begin{figure}[h]
\centering
\includegraphics[width=0.75\textwidth]{plateaucurve}
\caption{Plateau curve of scintillator 1, which is our target and is a representative example. It can be divided into three regions $(A, B, C)$, where $B$ is referred to as the plateau.}
\label{fig:plateau_curve}
\end{figure}

This curve can be divided into three regions $(A, B, C)$, where $B$ is referred to as the plateau.
The supply voltage should be set within this region, so that the count rate remains constant, even in case the supply voltage is changed within this region.
The wider the plateau region, the less the count rate will be affected by fluctuations in the dark current.
PMTs with better energy resolution and lower dark current pulses provide a wider plateau \cite{scintill}.

From the plateau curve one can read off where a noise contribution is expected.
In particular, for voltages above the plateau area there is a lot of thermal noise.
Combining the efficiency and the plateau measurement, one can see how the efficiency behaves while increasing the applied voltage and thus figure out the optimal working voltage, such that the scintillators have the highest efficiency at lowest noise contribution.
%The results for the plateau area and the voltage threshold $V(\epsilon_{\text{max}})$ in order to achieve maximum efficiency $\epsilon_{\text{max}}$ for each scintillator are shown in \autoref{tab:plat-effic}.
%
%\begin{table}[h]
%\centering
%\begin{tabular}{@{}ccc@{}}
%\toprule
%Scintillator & $V(\epsilon_{\text{max}})$ [V] & Plateau area [V] \\ \midrule
%1 & 2050 & 1950-2050 \\
%5 & 2100 & 1800-1900 \\
%6 & 2000 & (no data) \\
%7 & 2050 & 2000-2100 \\
%8 & 2100 & 1850-2000 \\
%9 & 2300 & 2300-2600 \\
%10 & 2200 & 2200-2400 \\
%11 & 2300 & 2200-2400 \\
%12 & 2100 & 2025-2100 \\
%13 & 2000 & 1900-2000 \\
%\bottomrule
%\end{tabular}
%\caption{Voltage at maximum efficiency $V(\epsilon_{\text{max}})$ and plateau area for each scintillator used in the setup.}
%\label{tab:plat-effic}
%\end{table}
%
%Finally, by comparison of the two columns one can figure out the optimal working voltage, such that the scintillators have the highest efficiency at lowest noise contribution. 

\subsection{TAC Calibration}
% Author: Dorothea II
% Comment: TAC2 here is the last one we used, called TAC3 in the logbook. This subsection is only a draft.

%% A BIT SHORT
Two Time to Amplitude Converters (TACs) have been used for the timing measurement.
Before their usage the calibration of the two devices has been performed. 
The calibration was done by measuring the ADC values corresponding to the TAC output voltages in the real readout chain by adding further delays with an intermediate delay box. 
The responses of both TACs is shown in \autoref{fig:taccal}. 
It is fairly linear in all but at the very beginning and end of the range, which will not be used.
This calibration was meant to be used for the analysis, but this was not possible due to unexpected shifts of the TAC offset voltages. 
The linearity and slope of the calibration do not seem to be affected by these shifts. 
How the calibration was done using the data is described in \autoref{subsec:timing_analysis}. 


\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.45\textwidth}
\includegraphics[width=\textwidth]{tac1cal}
\caption{TAC1 with range of $0.1 \si{\mu s}$.}
\label{fig:tac1_cal}
\end{subfigure}
\begin{subfigure}[t]{0.45\textwidth}
\includegraphics[width=\textwidth]{tac1cal}
\caption{TAC2 with range of $1 \si{\mu s}$.}
\label{fig:tac2_cal}
\end{subfigure}
\caption{Calibration curves of TAC1 and TAC2 respectively.}
\label{fig:taccal}
\end{figure}

%\begin{table}
%\centering
%  \begin{tabular}{| c | c | c | c | c |}
%    \cline{1-2}\cline{4-5}
%    \multicolumn{2}{|c|}{TAC 1} &  & \multicolumn{2}{|c|}{TAC 2} \\ \cline{1-2}\cline{4-5}
%    \textbf{Delay [ns]} & \textbf{ADC counts} & & \textbf{Delay [ns]} & \textbf{Voltage [V]} \\ \cline{1-2}\cline{4-5}
%    16 & 3000 & & 0 & 1.13 \\ \cline{1-2}\cline{4-5}%\hline
%    32 & 5600 & & 11 & 1.30 \\ \cline{1-2}\cline{4-5}%\hline
%    48 & 8000 & & 31 & 2.27 \\ \cline{1-2}\cline{4-5}%\hline
%    64 & 10500 & & 63 & 3.82 \\ \cline{1-2}\cline{4-5}%\hline
%    80 & 13000 & & 95 & 5.38 \\ \cline{1-2}\cline{4-5}%\hline
%    96 & 15000 & & 127 & 6.92 \\ \cline{1-2}\cline{4-5}%\hline
%    98 & 15500 & & & \\
%    \cline{1-2}\cline{4-5}%\hline
%  \end{tabular}
%  \caption{}
%  \label{table:TACs}
%\end{table}


\subsection{Data Acquisition System}
%%Author:Michael%%

The analog signals collected in the experiment are either used for trigger generation or recorded for offline analysis, whenever a trigger decision is made.
The Data Acquisition System (DAQ) we used consisted of two logic boxes.
The logic box is a Field Programmable Gate Array (FPGA) based data acquisition and control system developed at the Physikalisches Institut, Universität Heidelberg.

The FPGA main board comes with a USB connector for readout with a PC and LabView software, while providing 8 slots for additional I/O submodules.
We used boxes equipped with a 5 channel TTL or NIM I/O ("SU704") extension card, and seven Fast ADCs ("SU735") recording signals with up to 105 Megasamples per second and a resolution of 14bit, each.
This resolution is known to be limited by noise in the two least significant digits and only 12 bits are actually recorded by the FPGA.
The firmware of the I/O card is programmed such that the NIM channels have an input for the externally generated trigger and a \textit{busy} output signaling when the box will not be able to accept another trigger.

The logical OR of both busy signals acts as a trigger veto and keeps the boxes synchronized, such that they always record the same events.
To be able to verify this in the offline analysis of the recorded events, the logic boxes also save a timestamp to each of them. 

A LabView software was written to sort and combine the data samples of both boxes into a single binary file for each run, and at the same time it provided an event display for online monitoring and a first look at some statistics.
It also monitored the status of the LogicBoxes and allowed for some manipulations in their configuration.
The LabView software constantly received data from all channels of both boxes, filling them into two separate queues, first.
In each queue the data words had to be sorted, such that all data is stored event by event and channel after channel. 
Then the sorted queues are combined, again event by event. 
To minimize the loss of statistics in case of failures we had the data written to disk after a run consisting of 2000 events, with the software starting the next run automatically in case there were no problems. 
The corrupted runs either showed a different file size than expected or were unsynchronized and could be sorted out easily.
% unfortunately this happend quite often ...

\subsection{Trigger System}
\label{subsec:overview}
%Author: Gael

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{Everything_blockdiagram}
\caption{Schematics of the experimental setup. The 10 plastic scintillators are labeled using numbers. The target corresponds to scintillator 1 on the figure.}
\label{fig:setup}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{TriggerLogic}
\caption{Boolean logic branches indicating coincidences between scintillators shown in \autoref{fig:setup}, which construct the triggering mechanism. The timing for the trigger is defined by the scintillator in front of the calorimeter (5).}
\label{fig:logic}
\end{figure}

% \subsection{Experimental Setup}
%%Author:Colton%%
As mentioned in the very beginning of this chapter, two types of data need to be collected -- timing information and energy deposition.
By combining several scintillators into a trigger system for timing and calorimeter measurements, these data could be obtained.
\autoref{fig:setup} displays the physical layout of the experimental setup, which consists of a pion beam, a set of organic scintillators (labeled with numbers) and a calorimeter, while \autoref{fig:logic} shows the Boolean logic of the trigger.

With the goal of setting up the trigger system, the first step involves detecting the beam through coincidence of scintillators 10 and 9.
Here it is important that both scintillators are placed closely together and cover a majority of the beam hole, to minimize loss of statistics.
This coincidence is also placed in coincidence with scintillator 7, forming one condition for obtaining the rate of pions reaching the absorber.
The other condition is the beam radio frequency (RF), which is given by the accelerator at $50 \si{MHz}$ and needs to be tuned to the coincidence window of the first three scintillators (cf.~\autoref{fig:RF}).

As the beam is composed primarily of pions, but also contains signals from muons and electrons, the RF signal needs to be delayed in order to place the signal from the pions within the coincidence window.
As this should filter the muons and electrons from our beam signal, this coincidence rate $10\cdot9\cdot7\cdot \text{RF}$ is called $\pi$.

The pions need to be stopped in the target for the decay to be measured.
This is achieved by adding a plastic absorber where the pion's energy is degraded, such that the pions have just enough energy left to stop in the target.
Therefore, parallel to the $\pi$ coincidence, the target scintillator (1) and the scintillator after the absorber (8) are also placed in coincidence to ensure that the beam passes through our degrader.
Of importance here is, that the target has a small area, but greater thickness, which is supposed to generate a stronger signal response.
Placed along the beamline and behind the target, a much larger scintillator ($\overline{11}$) is used to veto pions which have passed through the target and will not decay in it.
Through the previous coincidences, this should guarantee that only pions which are stopped in the target are counted, preventing false signals or background.
The time when the pion stops is given by the target which yields the coincidence $\pi\text{-time}=10\cdot9\cdot7\cdot \text{RF}\cdot8\cdot1$. Taking the veto into account, the $\pi\text{-stop}$ is given by the total coincidence of $10\cdot9\cdot7\cdot\text{RF}\cdot8\cdot1\cdot\overline{11}$.
From now on, the line coming from the beam to the scintillator 11 will be referred to as the $\pi^+$-\textit{branch}.

With the goal of measuring the energy deposited by the $e^+$ coming from the stopped pion decay and the pion lifetime, a calorimeter is placed at the end of the scintillator chain, such that the pion decay products are incident on the calorimeter.
Therefore, two scintillators (5 and 6) roughly the size of the entrance window to the active material of the calorimeter, are placed in coincidence, such that only particles emitted from the target are gathered.
However, two more scintillators are used as vetoes.
Scintillator 12, as shown in \autoref{fig:setup} has a hole in its center to allow particles to pass, and is thus placed in anti-coincidence with 5 and 6.
Scintillator 13 is used as an active shielding to prevent particles emanating directly from the beam from giving a false signal in the calorimeter. 
The $e\text{-stop}$ coincidence is then defined by $5\cdot6\cdot\overline{12}\cdot\overline{13}$, where the timing is taken by scintillator 5.
This coincidence forms the electron portion, which will be addressed to as the $e^+$-\textit{branch}, concerning $e^+$ produced by the $\pi^+$ decaying in the target.
The overall trigger for the data acquisition (DAQ) is set by a wide coincidence between the $\pi\text{-stop}$ and the $e\text{-stop}$, i.e.~$\text{trigg} = \pi\text{-stop }\cdot e\text{-stop }$, where $\pi\text{-stop}$ waits $1\si{\mu s}$ for the signal of scintillator 5 to follow. This provides a correlation between the stopped $\pi^+$ and the $e^+$.

In order to measure the timing between the $\pi\text{-stop}$ and the trigger, Time to Amplitude Converter devices (TACs) are used.
One would intuitively set the $\pi\text{-stop}$ as the start and the trigger as the stop.
However, the stop could potentially never occur, since the $e^+$ could remain undetected.
Therefore the trigger, being the event with lowest frequency of the two, is connected to the start.
The $\pi\text{-stop}$ is delayed and connected to the stop. The resulting analogue pulse is then read out by the DAQ.
The decay curve shown in \autoref{fig:decaycurve} is expected.\par

\begin{figure}[h]
\centering
\includegraphics[scale = 0.5]{RF}
\caption{The narrow coincidence between $10\cdot9\cdot7$ (top) and the RF (middle) signal. This coincidence discriminates the $\pi^+$ and generate a square signal (bottom). }
\label{fig:RF}
\end{figure}
